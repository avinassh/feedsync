-- this table represents all the telegram groups/channels
CREATE TABLE IF NOT EXISTS groups
(
	pk 			INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    group_id 	TEXT 	UNIQUE NOT NULL
);
