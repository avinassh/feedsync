-- this table represents a RSS feed
CREATE TABLE IF NOT EXISTS feeds
(
	pk 			INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    url         TEXT 	UNIQUE NOT NULL
);
