-- this table maps feed subscriptions to a group
-- 
-- basically, a many to many relationship between
-- groups and feeds
CREATE TABLE IF NOT EXISTS subscriptions
(
	pk 			INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    feed_id 	INTEGER REFERENCES feeds(pk) NOT NULL,
    group_id 	INTEGER REFERENCES groups(pk) NOT NULL,

    UNIQUE(feed_id,group_id)
);
