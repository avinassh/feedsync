use std::env;
use std::error::Error;

use rss::Channel;
use telegram_bot::*;
use tokio::stream::Stream;
use tokio::stream::StreamExt;

use feedsync::storage::Store;
use std::time::Duration;

struct Engine {
    store: Store,
}

async fn fetch_feed(url: String) -> Result<Channel, Box<dyn Error>> {
    let client = reqwest::Client::builder()
        .timeout(Duration::from_secs(5))
        .build()?;
    let content = client.get(&url).send().await?.bytes().await?;
    let channel = Channel::read_from(&content[..])?;
    Ok(channel)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let token = env::var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");
    let api = Api::new(token);

    let s = Store::default();

    // Fetch new updates via long poll method
    let mut stream = api.stream();
    while let Some(update) = stream.next().await {
        // If the received update contains a new message...
        let update = update?;
        if let UpdateKind::Message(message) = update.kind {
            if let MessageKind::Text { data, .. } = message.kind.clone() {
                let cmd = data.split_whitespace().next().unwrap();
                let arg = data.split_whitespace().next_back().unwrap();
                // Print received text message to stdout.
                if cmd.starts_with("/all") {
                    api.send(message.text_reply(format!(
                        "You: {}, requested all data",
                        &message.from.first_name
                    )))
                    .await?;
                }
                if cmd.starts_with("/subscribe") {
                    match fetch_feed(String::from(arg)).await {
                        Err(err) => {
                            api.send(
                                message.text_reply(format!("failed to subscribe to feed {}", err)),
                            )
                            .await?;
                        }
                        Ok(_) => {
                            s.add_subscription(arg.to_string(), message.chat.id().to_string())
                                .await;
                            api.send(message.text_reply(format!("subscribed successfully")))
                                .await?;
                        }
                    }
                }
                if cmd.starts_with("/unsubscribe") {}

                // println!(
                //     "<{}>: {}",
                //     &message.chat.id(),
                //     data.split_whitespace().next().unwrap().starts_with("/all")
                // );
                // println!("<{}>: {:?}", &message.chat.id(), entities);
            }
        }
    }
    Ok(())
}
