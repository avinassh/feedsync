use std::env;
use std::fmt::Debug;

use sqlx::error::DatabaseError;
use sqlx::sqlite::{SqliteError, SqlitePool};
use sqlx::{Pool, Sqlite};

const SQLITE_UNIQUE_CONSTRAINT_FAILED_ERR_CODE: &str = "2067";

#[derive(Debug)]
pub struct Store {
    pool: Pool<Sqlite>,
}

#[derive(Debug)]
pub struct Subscription {
    id: i64,
    url: String,
}

impl Default for Store {
    fn default() -> Self {
        Store {
            pool: SqlitePool::connect_lazy(&env::var("DATABASE_URL").unwrap()).unwrap(),
        }
    }
}

impl Store {
    pub async fn add_subscription(&self, feed_url: String, group_id: String) -> Result<(), String> {
        let feed_id = self
            .get_or_create_feed(feed_url)
            .await
            .map_err(|e| e.to_string())?;
        let group_id = self
            .get_or_create_group(group_id)
            .await
            .map_err(|e| e.to_string())?;
        let result = sqlx::query!(
            r#"INSERT INTO subscriptions (feed_id, group_id) VALUES ( ?1, ?2 )"#,
            feed_id,
            group_id
        )
        .execute(&self.pool)
        .await;

        match result {
            Err(err) => {
                // we ignore unique constraint fail errors
                let e: &SqliteError = err
                    .as_database_error()
                    .unwrap()
                    .downcast_ref::<SqliteError>();
                if e.code().as_deref() == Some(SQLITE_UNIQUE_CONSTRAINT_FAILED_ERR_CODE) {
                    return Ok(());
                }
                Err(e.to_string())
            }
            Ok(_) => Ok(()),
        }
    }

    async fn get_or_create_feed(&self, feed_url: String) -> Result<i64, sqlx::Error> {
        let result: Result<_, sqlx::Error> =
            sqlx::query!(r#"INSERT INTO feeds ( url ) VALUES ( ?1 )"#, feed_url)
                .execute(&self.pool)
                .await;
        match result {
            Err(err) => {
                // we ignore unique constraint fail errors
                let e = err
                    .as_database_error()
                    .unwrap()
                    .downcast_ref::<SqliteError>();
                if e.code().as_deref() == Some(SQLITE_UNIQUE_CONSTRAINT_FAILED_ERR_CODE) {
                    // return the existing row
                    return Ok(self.get_feed(feed_url).await);
                }
                Err(err)
            }
            Ok(result) => Ok(result.last_insert_rowid()),
        }
    }

    async fn get_feed(&self, feed_url: String) -> i64 {
        let result: Result<_, sqlx::Error> =
            sqlx::query!("SELECT pk from feeds where url = ?", feed_url)
                .fetch_one(&self.pool)
                .await;
        // unwrap is okay, since get_feed is always called after making sure `pk` exists
        result.unwrap().pk
    }

    async fn get_or_create_group(&self, group_id: String) -> Result<i64, sqlx::Error> {
        let result: Result<_, sqlx::Error> =
            sqlx::query!(r#"INSERT INTO groups ( group_id ) VALUES ( ?1 )"#, group_id)
                .execute(&self.pool)
                .await;
        match result {
            Err(err) => {
                // we ignore unique constraint fail errors
                let e = err
                    .as_database_error()
                    .unwrap()
                    .downcast_ref::<SqliteError>();
                if e.code().as_deref() == Some(SQLITE_UNIQUE_CONSTRAINT_FAILED_ERR_CODE) {
                    // return the existing row
                    return Ok(self.get_group(group_id).await);
                }
                Err(err)
            }
            Ok(result) => Ok(result.last_insert_rowid()),
        }
    }

    async fn get_group(&self, group_id: String) -> i64 {
        let result: Result<_, sqlx::Error> =
            sqlx::query!("SELECT pk from groups where group_id = ?", group_id)
                .fetch_one(&self.pool)
                .await;
        // unwrap is okay, since get_feed is always called after making sure `pk` exists
        result.unwrap().pk
    }

    pub async fn get_all_subscriptions(
        &self,
        group_id: String,
    ) -> Result<Vec<Subscription>, String> {
        let results: Vec<Subscription> = sqlx::query_as!(
            Subscription,
            "select subscriptions.pk as id, feeds.url from subscriptions inner join feeds on subscriptions.feed_id = feeds.pk join groups on subscriptions.group_id = groups.pk where groups.group_id = ?",
            group_id
        )
        .fetch_all(&self.pool).await.map_err(|e| e.to_string())?;
        Ok(results)
    }

    pub async fn remove_subscription_by_id(
        &self,
        subscription_id: String,
        group_id: String,
    ) -> Result<(), String> {
        let results = sqlx::query!(
        r#"select exists (select subscriptions.pk from subscriptions inner join groups on subscriptions.group_id = groups.pk where groups.group_id = ? and subscriptions.pk = ?) as "exists!""#,
        group_id, subscription_id)
            .fetch_one(&self.pool)
            .await
            .map_err(|e| e.to_string())?;
        if results.exists == 0 {
            return Err("subscription does not exist".to_string());
        }
        sqlx::query!("delete from subscriptions where pk = ?", subscription_id)
            .fetch_all(&self.pool)
            .await
            .map_err(|e| e.to_string())?;
        Ok(())
    }
}
